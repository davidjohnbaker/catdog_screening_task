def get_stimulus_set(self, media_url: str):
        return StimulusSet(
            "headphone_check",
            [
                StimulusSpec(
                    definition={
                        "label": label,
                        "correct_answer": answer,
                        "url": f"{media_url}/antiphase_HC_{label}.wav",
                    },
                    phase="screening",
                )
                for label, answer in [
                    ("ISO", "2"),
                    ("IOS", "3"),
                    ("SOI", "1"),
                    ("SIO", "1"),
                    ("OSI", "2"),
                    ("OIS", "3"),
                ]
            ],
        )
