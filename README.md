# Cat Dog Screening Task 

This repository contains a small project developed DJB's on-boarding to the 
PsyNet project as part of the Computational Audition group at MPI.

TODO for Dave

* [ ] Figure out blocking w list comprehension
* [ ] Add Instructional Pages 

Current Workflow Plan 

* [X] Create Small Indp Repo for Project
* [X] Launch basics non-adaptive from repo
* [X] Copy in Nori Dict Code
* [X] Sketch out logic for scoring 
* [X] Implement attempt at scoring
