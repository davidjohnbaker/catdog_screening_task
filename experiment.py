# pylint: disable=unused-import,abstract-method

##########################################################################################
# Imports
##########################################################################################

import logging

from flask import Markup

import psynet.experiment
from psynet.modular_page import ModularPage, PushButtonControl
from psynet.page import InfoPage, SuccessfulEndPage
from psynet.timeline import Timeline
from psynet.trial.non_adaptive import (
    NonAdaptiveTrial,
    NonAdaptiveTrialMaker,
    StimulusSet,
    StimulusSpec,
    StimulusVersionSpec,
)

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger()


##########################################################################################
# Stimuli
##########################################################################################

# Point 1: Updated default list to be dict that holds label and answer

stimulus_set = StimulusSet(
    "animals",
    [
        StimulusSpec(
            definition={
                "label": animal,
                "correct_answer": answer},  
            version_specs=[
                StimulusVersionSpec(definition={"text_color": text_color})
                for text_color in ["black"]
            ],
            phase="experiment",
            block="primary", 
        )
        # Point 2: added in tuple instead of list for dict scoring 
        for animal, answer in [
            ("likes to sleep around the clock.","cat"), 
            ("loves being called a good boy.","dog"),
            ("eats fish.", "cat"),
            ("eats literally anything.", "dog"),
            ("lives underwater.","not a mammal"),
            ]
    ]
)


class AnimalTrial(NonAdaptiveTrial):
    __mapper_args__ = {"polymorphic_identity": "animal_trial"}

    # num_pages = 2

    def show_trial(self, experiment, participant):
        text_color = self.definition["text_color"]
        animal = self.definition["label"] 
        block = self.block

        header = f"<h4 id='trial-position'>Trial {self.position + 1}</h3>"

        header = header + f"<h4>Block {block}</h3>"

        # Point 3: Modified presentation text and above definitions 
        page = ModularPage(
            "animal_trial",
            Markup(
                f"""
                {header}
                <p style='color: {text_color}'>I am thinking of a little creature that {animal}</p>
                """
            ),
            PushButtonControl(["cat", "dog", "not a mammal"]),
        )

        return page

    # Added fprint to show what correct answer was (just to learn) 
    def show_feedback(self, experiment, participant):
        return InfoPage(f"You responded '{self.answer}'. The correct answer was '{self.definition['correct_answer']}' ")


class AnimalTrialMaker(NonAdaptiveTrialMaker):
    def performance_check(self, experiment, participant, participant_trials):
        """Should return a tuple (score: float, passed: bool)"""
        score = 0
        # Point 4: updated scoring logic to reflect dict lookup (see nj_examples) 
        for trial in participant_trials:
            if trial.answer == trial.definition["correct_answer"]:
                score += 1
        passed = score >= 4 # Point 5: Not sure where this should be declared  
        return {"score": score, "passed": passed}

    give_end_feedback_passed = True

    def get_end_feedback_passed_page(self, score):
        return InfoPage(
            Markup(f"You finished the animal questions! Your score was {score}."),
            time_estimate=5,
        )


# Point 6: What scope should this be declared?  
start_here = InfoPage(Markup(f"<p>This is where the text at the start would go.</p><p>You will be given some descriptions, you need to match the text with the animal.</p>"), time_estimate=3)

trial_maker = AnimalTrialMaker(
    id_="animals",
    trial_class=AnimalTrial,
    phase="experiment",
    stimulus_set=stimulus_set,
    time_estimate_per_trial=3,
    max_trials_per_block=6,
    allow_repeated_stimuli=False,
    max_unique_stimuli_per_block=None,
    active_balancing_within_participants=False,
    active_balancing_across_participants=False,
    check_performance_at_end=True,
    check_performance_every_trial=False,
    target_num_participants=1,
    target_num_trials_per_stimulus=None,
    recruit_mode="num_participants",
    num_repeat_trials=0,
)

##########################################################################################
# Experiment
##########################################################################################

# TASK: eventually hae 4/6 with correct answer to pass the text 
# NOTE: Update to have 2 blocks just so could expeirment with this 

# Weird bug: if you instead import Experiment from psynet.experiment,
# Dallinger won't allow you to override the bonus method
# (or at least you can override it but it won't work).
class Exp(psynet.experiment.Experiment):
    consent_audiovisual_recordings = False

    timeline = Timeline(start_here, trial_maker, SuccessfulEndPage())

    def __init__(self, session=None):
        super().__init__(session)
        self.initial_recruitment_size = 1 # will eventually be 11 


extra_routes = Exp().extra_routes()
